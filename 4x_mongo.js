var mongo = require('mongodb');
 
var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;
 
var server = new Server('localhost', 27017, {auto_reconnect: true});
db = new Db('ForexDB', server);
//TODO : implement db authentication
//db.authenticate('4xuser','passw0rd' , {authdb: 'ForexDB'}, function(err, result) {
//    client.collection('ForexDB', update);
//});
db.open(function(err, db) {
    if(!err) {
        console.log("Connected to 'ForexDB' database");
        db.collection('ForexDB', {strict:true}, function(err, collection) {
            if (err) {
                console.log("The 'ForexDB' collection doesn't exist. Creating it with sample data...");
                //populateDB();
            }
        });
    }
});
 
exports.findBySymbol = function(base) {
    //var base = req.params.base;
    console.log('Retrieving currency: ' + base);
    db.collection('ForexDB', function(err, collection) {
        collection.findOne({'base':base}, function(err, item) {
            res.send(item);
        });
    });
};
 
exports.findAllSymbols = function(req, res) {
    db.collection('ForexDB', function(err, collection) {
        collection.find().toArray(function(err, items) {
            res.send(items);
        });
    });
};
 
exports.addSymbol = function(oeResponse) {
   // var oeReponse = req.body;
   console.log('Adding currency: ' + JSON.stringify(oeResponse));
   oeResponse
    db.collection('ForexDB', function(err, collection) {
        collection.insert(oeResponse, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred'});
            } else {
                console.log('Success: ' + JSON.stringify(result[0]));
     //           res.send(result[0]);
            }
        });
    });
}

/* 
exports.updateSymbol = function(base,oeResponse) {
  //  var base = req.params.base;
    //var timestamp = req.params.timestamp;
  //  var oeResponse = req.body;
    console.log('Updating currency: ' + base);
    console.log(JSON.stringify(oeResponse));

    db.collection('ForexDB', function(err, collection) {
        collection.update({'base':base}, oeResponse, {safe:true}, function(err, result) {
            if (err) {
                console.log('Error updating currency: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('' + result + ' document(s) updated');
                res.send(oeResponse);
            }
        });
    });
}
*/
/*

//There shall not be a need for deleting the currency symbol. 
exports.deleteSymbol = function(req, res) {
    var base = req.params.base;
    console.log('Deleting currency: ' + base);
    db.collection('ForexDB', function(err, collection) {
        collection.remove({'base':base}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}
*/ 
 
 /*--------------------------------------------------------------------------------------------------------------------*/
// Populate database with sample data -- Only used once: the first time the application is started.
// You'd typically not find this code in a real-life app, since the database would already exist.
/*var populateDB = function() {
 
    var symbols = [
    { disclaimer: 'Exchange rates are provided for informational purposes only, and do not constitute financial advice of any kind. Although every attempt is made to ensure quality, NO guarantees are given whatsoever of accuracy, validity, availability, or fitness for any purpose - please use at your own risk. All usage is subject to your acceptance of the Terms and Conditions of Service, available at: https://openexchangerates.org/terms/',
  license: 'Data sourced from various providers with public-facing APIs; copyright may apply; resale is prohibited; no warranties given of any kind. All usage is subject to your acceptance of the License Agreement available at: https://openexchangerates.org/license/',
  timestamp: 1367629268,
  base: 'USD',
  rates: 
   { AED: 3.672983,
     AFN: 53.786667,
     ALL: 107.355001,
     AMD: 415.531251,
     ANG: 1.7887,
     AOA: 96.0381,
     ARS: 5.196927,
     AUD: 0.970067,
     AWG: 1.7911,
     AZN: 0.7847,
     BAM: 1.493088,
     BBD: 2,
     BDT: 78.235388,
     BGN: 1.494197,
     BHD: 0.377013,
     BIF: 1571.96,
     BMD: 1,
     BND: 1.235586,
     BOB: 6.997517,
     BRL: 2.007679,
     BSD: 1,
     BTC: 0.010958,
     BTN: 53.9321,
     BWP: 8.098609,
     BYR: 8687.5225,
     BZD: 1.986371,
     CAD: 1.010684,
     CDF: 919.696307,
     CHF: 0.936659,
     CLF: 0.02051,
     CLP: 470.423044,
     CNY: 6.167881,
     COP: 1836.29061,
     CRC: 500.683701,
     CUP: 22.687419,
     CVE: 84.344837,
     CZK: 19.598009,
     DJF: 177.218374,
     DKK: 5.692653,
     DOP: 41.077986,
     DZD: 78.770203,
     EEK: 11.760122,
     EGP: 6.938198,
     ETB: 18.61425,
     EUR: 0.762548,
     FJD: 1.775431,
     FKP: 0.643061,
     GBP: 0.643061,
     GEL: 1.649075,
     GHS: 1.972578,
     GIP: 0.643061,
     GMD: 33.054012,
     GNF: 7113.873333,
     GTQ: 7.788958,
     GYD: 202.801666,
     HKD: 7.758602,
     HNL: 19.265245,
     HRK: 5.786594,
     HTG: 42.556725,
     HUF: 225.713126,
     IDR: 9729.055715,
     ILS: 3.570671,
     INR: 53.883902,
     IQD: 1163.980018,
     IRR: 12300.9,
     ISK: 116.122501,
     JEP: 0.643061,
     JMD: 98.92391,
     JOD: 0.708282,
     JPY: 98.737206,
     KES: 83.787343,
     KGS: 48.12,
     KHR: 4009.366667,
     KMF: 375.595673,
     KPW: 900,
     KRW: 1101.337773,
     KWD: 0.284473,
     KYD: 0.822352,
     KZT: 151.288317,
     LAK: 7671.943268,
     LBP: 1506.65735,
     LKR: 126.456189,
     LRD: 74.760167,
     LSL: 8.956958,
     LTL: 2.637574,
     LVL: 0.534923,
     LYD: 1.276919,
     MAD: 8.490901,
     MDL: 12.275755,
     MGA: 2175.121667,
     MKD: 47.117143,
     MMK: 891.787875,
     MNT: 1428.5,
     MOP: 8.003051,
     MRO: 264.976495,
     MTL: 0.683602,
     MUR: 30.983013,
     MVR: 15.392263,
     MWK: 399.464667,
     MXN: 12.083056,
     MYR: 3.035863,
     MZN: 29.953333,
     NAD: 8.953523,
     NGN: 158.262069,
     NIO: 24.288671,
     NOK: 5.811178,
     NPR: 86.502676,
     NZD: 1.173457,
     OMR: 0.38502,
     PAB: 1,
     PEN: 2.636129,
     PGK: 2.137333,
     PHP: 40.935123,
     PKR: 98.394872,
     PLN: 3.159691,
     PYG: 4159.481668,
     QAR: 3.640343,
     RON: 3.293402,
     RSD: 84.574103,
     RUB: 31.114802,
     RWF: 640.042619,
     SAR: 3.750205,
     SBD: 7.254446,
     SCR: 11.722683,
     SDG: 4.419285,
     SEK: 6.520213,
     SGD: 1.233849,
     SHP: 0.643061,
     SLL: 4319.948277,
     SOS: 1514.343333,
     SRD: 3.28125,
     STD: 18651,
     SVC: 8.757205,
     SYP: 70.1939,
     SZL: 8.961885,
     THB: 29.59829,
     TJS: 4.757,
     TMT: 2.85015,
     TND: 1.618487,
     TOP: 1.741595,
     TRY: 1.794314,
     TTD: 6.425692,
     TWD: 29.5499,
     TZS: 1627.401995,
     UAH: 8.116527,
     UGX: 2580.302765,
     USD: 1,
     UYU: 18.984976,
     UZS: 2063.425815,
     VEF: 6.293004,
     VND: 20921.470825,
     VUV: 91.199999,
     WST: 2.290963,
     XAF: 500.428485,
     XAG: 0.042325,
     XAU: 0.000696,
     XCD: 2.701275,
     XDR: 0.6623,
     XOF: 500.670002,
     XPF: 91.246188,
     YER: 215.00442,
     ZAR: 8.953569,
     ZMK: 5227.108333,
     ZMW: 5.32181,
     ZWL: 322.387247} }];
 
    db.collection('ForexDB', function(err, collection) {
        collection.insert(symbols, {safe:true}, function(err, result) {
        	
        	if (err) {
                console.log({'error':'An error has occurred - ' + err});
            } else {
            	console.log(' data added successfully');
            }
        });
    });
 
};
*/